package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 * 用户实体
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 要求数据库选择自增方式
    private  Integer id;

    private  String username;

    private  String password;

    private Double firstCommission;

    private Double firstPrincipal;

    private String todayFirstOrderTime;

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Double getFirstCommission() {
        return firstCommission;
    }

    public void setFirstCommission(Double firstCommission) {
        this.firstCommission = firstCommission;
    }

    public Double getFirstPrincipal() {
        return firstPrincipal;
    }

    public void setFirstPrincipal(Double firstPrincipal) {
        this.firstPrincipal = firstPrincipal;
    }

    public String getTodayFirstOrderTime() {
        return todayFirstOrderTime;
    }

    public void setTodayFirstOrderTime(String todayFirstOrderTime) {
        this.todayFirstOrderTime = todayFirstOrderTime;
    }

}
