package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 */
import com.lzj.lzjServer.enums.StatusCode;
import com.lzj.lzjServer.request.OrderByUsername;
import com.lzj.lzjServer.request.UserParams;
import com.lzj.lzjServer.response.MessageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    /**
     * 查找所有用户
     * @return
     */
    @GetMapping(value = "/users")
    public List<User> userList() {
        return userRepository.findAll();
    }

    /**
     * 添加一个用户
     * @param userParams
     * @return
     */
    @PostMapping(value = "/users")
    public MessageWrapper<User> userAdd(@RequestBody UserParams userParams) {
        List<String> allUser = new ArrayList();

        for (User str : userRepository.findAll()) {
            allUser.add(str.getUsername());
//            System.out.println(str.getUsername());
        }

        if (allUser.indexOf(userParams.getUsername()) == -1) {
            User user = new User();
            user.setUsername(userParams.getUsername());
            user.setPassword(userParams.getPassword());
            userRepository.save(user); // 保存用户

            return new MessageWrapper<User>().wrapMessage(StatusCode.SUCCESS, "用户添加成功!");
        } else {
            return new MessageWrapper<User>().wrapMessage(StatusCode.ERROR, "用户名已存在，请重新输入!");
        }
    }

    /**
     * 更新一个用户
     * @param id
     * @param userParams
     * @return
     */
    @PutMapping(value = "/users/{id}")
    public User userUpdate(@PathVariable("id") Integer id,
                           @RequestBody UserParams userParams) {
        User user = new User();
        user.setId(id);
        user.setUsername(userParams.getUsername());
        user.setPassword(userParams.getPassword());

        return userRepository.save(user);
    }

    /**
     * 删除一个用户
     * @param id
     */
    @DeleteMapping(value = "/users/{id}")
    public MessageWrapper userDelete(@PathVariable("id") Integer id) {
        MessageWrapper messageWrapper = new MessageWrapper();
        userRepository.deleteById(id);
        messageWrapper.wrapMessage("200", "Success");
        return messageWrapper;
    }

    /**
     * 获取一个用户
     * @param id
     * @return
     */
    @GetMapping(value = "/users/{id}")
    public User getOne (@PathVariable("id") Integer id) {
        return userRepository.getOne(id);
    }

    /**
     * 通过用户名查找用户的第一次佣金和本金
     * @param orderByUsername
     * @return
     */
    @PostMapping(value = "/firstMoney/username")
    public User findUserByUsername(@RequestBody OrderByUsername orderByUsername) {
        return userRepository.findByUsername(orderByUsername.getUsername());
    }

    /**
     * 更新一个用户的第一次佣金和本金
     * @param id
     * @param userParams
     * @return
     */
    @PutMapping(value = "/users/firstMoney/{id}")
    public User userCommissionUpdate(@PathVariable("id") Integer id,
                           @RequestBody UserParams userParams) {
        User user = new User();
        user.setId(id);
        user.setUsername(userParams.getUsername());
        user.setPassword(userParams.getPassword());
        user.setFirstCommission(userParams.getFirstCommission());
        user.setFirstPrincipal(userParams.getFirstPrincipal());
        user.setTodayFirstOrderTime(userParams.getTodayFirstOrderTime());

        return userRepository.save(user);
    }
}
