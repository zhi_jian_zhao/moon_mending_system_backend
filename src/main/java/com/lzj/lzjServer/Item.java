package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 * 订单实体
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Entity
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 要求数据库选择自增方式
    private  Integer id;

    private  String orderTime;

    private  String username;

    private  String orderNumber;

    private Double orderPrincipal;

    private Double orderCommission;

    private String orderRemarks;

    private String operator;

    private String others;

    public Item() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Double getOrderPrincipal() {
        return orderPrincipal;
    }

    public void setOrderPrincipal(Double orderPrincipal) {
        this.orderPrincipal = orderPrincipal;
    }

    public Double getOrderCommission() {
        return orderCommission;
    }

    public void setOrderCommission(Double orderCommission) {
        this.orderCommission = orderCommission;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }
}
