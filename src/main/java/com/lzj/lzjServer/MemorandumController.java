package com.lzj.lzjServer;

import com.lzj.lzjServer.response.MessageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ZhiJian.Zhao
 * @since 2019-03-06
 * 备忘录
 */
@RestController
@RequestMapping("/api/v1")
public class MemorandumController {
    @Autowired
    private MemorandumRepository memorandumRepository;

    /**
     * 查找所有备忘录
     * @return
     */
    @GetMapping(value = "/memorandums")
    public List<Memorandum> memorandumList() {
        return memorandumRepository.findAll();
    }

    /**
     * 添加一个备忘录
     * @param memorandumParams
     * @return
     */
    @PostMapping(value = "/memorandum")
    public Memorandum memorandumAdd(@RequestBody Memorandum memorandumParams) {

        Memorandum memorandum = new Memorandum();
        memorandum.setTitle(memorandumParams.getTitle());
        memorandum.setTime(memorandumParams.getTime());
        memorandum.setImg(memorandumParams.getImg());
        memorandum.setContent(memorandumParams.getContent());

        return memorandumRepository.save(memorandum);
    }

    /**
     * 更新一个备忘录
     * @param id
     * @param memorandumParams
     * @return
     */
    @PutMapping(value = "/memorandum/{id}")
    public Memorandum memorandumUpdate(@PathVariable("id") Integer id,
                           @RequestBody Memorandum memorandumParams) {
        Memorandum memorandum = new Memorandum();
        memorandum.setId(id);
        memorandum.setTitle(memorandumParams.getTitle());
        memorandum.setTime(memorandumParams.getTime());
        memorandum.setImg(memorandumParams.getImg());
        memorandum.setContent(memorandumParams.getContent());

        return memorandumRepository.save(memorandum);
    }

    /**
     * 删除一个备忘录
     * @param id
     */
    @DeleteMapping(value = "/memorandum/{id}")
    public MessageWrapper memorandumDelete(@PathVariable("id") Integer id) {
        MessageWrapper messageWrapper = new MessageWrapper();
        memorandumRepository.deleteById(id);
        messageWrapper.wrapMessage("200", "Success");
        return messageWrapper;
    }

    /**
     * 获取一个备忘录
     * @param id
     * @return
     */
    @GetMapping(value = "/memorandum/{id}")
    public Memorandum getOne (@PathVariable("id") Integer id) {
        return memorandumRepository.getOne(id);
    }
}
