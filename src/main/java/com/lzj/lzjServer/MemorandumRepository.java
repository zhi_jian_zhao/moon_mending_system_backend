package com.lzj.lzjServer;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ZhiJian.Zhao
 * @since 2019-03-06
 * 备忘录
 */
public interface MemorandumRepository extends JpaRepository<Memorandum,Integer> {
}
