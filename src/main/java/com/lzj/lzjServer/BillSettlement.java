package com.lzj.lzjServer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-17
 * 订单结算
 */
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Entity
public class BillSettlement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 要求数据库选择自增方式
    private  Integer id;

    private  String username;

    @Column(length = 1000)
    private String settlementDay;

    public BillSettlement() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSettlementDay() {
        return settlementDay;
    }

    public void setSettlementDay(String settlementDay) {
        this.settlementDay = settlementDay;
    }
}
