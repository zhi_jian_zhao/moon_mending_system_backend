package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 */
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;

import java.util.List;
//import com.lzj.lzjServer.response.OrderSum;

public interface ItemRepository extends JpaRepository<Item,Integer> {
    // 通过用户名来查询
    public List<Item> findByUsername(String username);

    // 通过operator来查询
    public List<Item> findByOperator(String operator);

//    @Query(value = "select  * FROM item", nativeQuery = true)
//    public List<Item> sumBill();

//    @Query(value = "select DATE_FORMAT(item.order_time,'%Y-%m-%d') AS day, COUNT(*) AS orderCount, SUM(item.order_principal) AS principal,\n" +
//            " SUM(item.order_commission) AS commission FROM item GROUP BY(DATE_FORMAT(item.order_time,'%Y-%m-%d'))", nativeQuery = true)
//    public List<OrderSum> sumBill();
}
