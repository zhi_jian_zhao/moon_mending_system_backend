package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 */
import com.lzj.lzjServer.request.OrderByUsername;
import com.lzj.lzjServer.request.OrderParams;
import com.lzj.lzjServer.response.MessageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.lzj.lzjServer.response.OrderSum;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ItemController {
    @Autowired
    private ItemRepository itemRepository;

    /**
     * 查找所有订单
     * @return
     */
    @GetMapping(value = "/orders")
    public List<Item> orderList() {
        return itemRepository.findAll();
    }

    /**
     * 添加一个订单
     * @param orderParams
     * @return
     */
    @PostMapping(value = "/orders")
    public Item orderAdd(@RequestBody OrderParams orderParams) {
        Item order = new Item();
        order.setOrderTime(orderParams.getOrderTime());
        order.setUsername(orderParams.getUsername());
        order.setOrderNumber(orderParams.getOrderNumber());
        order.setOrderPrincipal(orderParams.getOrderPrincipal());
        order.setOrderCommission(orderParams.getOrderCommission());
        order.setOrderRemarks(orderParams.getOrderRemarks());
        order.setOperator(orderParams.getOperator());

        return itemRepository.save(order);
    }

    /**
     * 更新一个订单
     * @param orderParams
     * @return
     */
    @PutMapping(value = "/orders/{id}")
    public Item orderUpdate(@PathVariable("id") Integer id,
                            @RequestBody OrderParams orderParams) {
        Item order = new Item();
        order.setId(id);
        order.setOrderTime(orderParams.getOrderTime());
        order.setUsername(orderParams.getUsername());
        order.setOrderNumber(orderParams.getOrderNumber());
        order.setOrderPrincipal(orderParams.getOrderPrincipal());
        order.setOrderCommission(orderParams.getOrderCommission());
        order.setOrderRemarks(orderParams.getOrderRemarks());
        order.setOperator(orderParams.getOperator());
        order.setOthers(orderParams.getOthers());

        return itemRepository.save(order);
    }

    /**
     * 删除一个订单
     * @param id
     */
    @DeleteMapping(value = "/orders/{id}")
    public MessageWrapper orderDelete(@PathVariable("id") Integer id) {
        itemRepository.deleteById(id);

        MessageWrapper messageWrapper = new MessageWrapper();
        messageWrapper.wrapMessage("200", "Success");
        return messageWrapper;
    }

    /**
     * 通过用户名查找订单
     * @param orderByUsername
     * @return
     */
    @PostMapping(value = "/orders/username")
    public List<Item> orderListByUsername(@RequestBody OrderByUsername orderByUsername) {
        return itemRepository.findByUsername(orderByUsername.getUsername());
    }

    /**
     * 通过管理员名（operator）查找订单
     * @param operator
     * @return
     */
    @GetMapping(value = "/ordersByOperator/{operator}")
    public List<Item> orderListByOperator(@PathVariable("operator") String operator) {
        return itemRepository.findByOperator(operator);
    }

    /**
     * 订单统计
     * @return
     */
//    @GetMapping(value = "/ordersStatistics")
//    public List<OrderSum> ordersStatistics() {
//        return itemRepository.sumBill();
//    }
//    public List<Item> ordersStatistics() {
//        return itemRepository.sumBill();
//    }
}
