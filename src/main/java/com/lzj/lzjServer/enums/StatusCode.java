package com.lzj.lzjServer.enums;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 */
public class StatusCode {

  public static final String SUCCESS = "200";
  public static final String ERROR = "201";
  public static final String INVALID = "202";
  public static final String INVALID_PARAMS = "401";
  public static final String RECREATE="222";

}
