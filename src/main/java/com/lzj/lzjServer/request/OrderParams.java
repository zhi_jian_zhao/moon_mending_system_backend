package com.lzj.lzjServer.request;

public class OrderParams {
    private  String orderTime;

    private  String username;

    private  String orderNumber;

    private Double orderPrincipal;

    private Double orderCommission;

    private String orderRemarks;

    private String operator;

    private String others;

    public String getOrderTime() {
        return orderTime;
    }

    public String getUsername() {
        return username;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public Double getOrderPrincipal() {
        return orderPrincipal;
    }

    public Double getOrderCommission() {
        return orderCommission;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public String getOperator() {
        return operator;
    }

    public String getOthers() {
        return others;
    }
}
