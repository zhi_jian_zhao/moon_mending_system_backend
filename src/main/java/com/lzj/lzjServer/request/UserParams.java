package com.lzj.lzjServer.request;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-10
 */
public class UserParams {
    private String username;
    private String password;
    private Double firstCommission;
    private Double firstPrincipal;
    private String todayFirstOrderTime;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Double getFirstCommission() {
        return firstCommission;
    }

    public Double getFirstPrincipal() {
        return firstPrincipal;
    }

    public String getTodayFirstOrderTime() {
        return todayFirstOrderTime;
    }
}
