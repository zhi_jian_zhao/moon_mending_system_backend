package com.lzj.lzjServer.request;

public class SettlementParams {
    private String username;

    private String settlementDay;

    public String getUsername() {
        return username;
    }

    public String getSettlementDay() {
        return settlementDay;
    }
}
