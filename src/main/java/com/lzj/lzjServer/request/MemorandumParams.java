package com.lzj.lzjServer.request;

/**
 * @author ZhiJian.Zhao
 * @since 2019-03-06
 * 备忘录参数
 */
public class MemorandumParams {
    private  String title;

    private  String time;

    private String content;

    private String img;

    public String getTitle() {
        return title;
    }

    public String getTime() {
        return time;
    }

    public String getContent() {
        return content;
    }

    public String getImg() {
        return img;
    }
}
