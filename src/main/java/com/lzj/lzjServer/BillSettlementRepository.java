package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-17
 * 订单结算
 */
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillSettlementRepository extends JpaRepository<BillSettlement,Integer> {
    // 通过用户名来查询
    public BillSettlement findByUsername(String username);
}
