package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-07
 */
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    // 通过用户名来查询
    public User findByUsername(String username);
}
