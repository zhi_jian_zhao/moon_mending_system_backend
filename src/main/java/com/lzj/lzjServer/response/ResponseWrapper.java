package com.lzj.lzjServer.response;

public interface ResponseWrapper<T extends Object> {
    String getCode();

    T getData();

    String getMessage();
}
