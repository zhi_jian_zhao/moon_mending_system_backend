package com.lzj.lzjServer.response;

public class MessageWrapper<T extends Object> implements ResponseWrapper<T> {

    private String code;
    private T data;
    private String message;

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageWrapper<T> wrapMessage(String code, String message) {
        wrapMessage(code, message, null);
        return this;
    }

    public MessageWrapper<T> wrapMessage(String code, String msg, T data) {
        setCode(code);
        setData(data);
        setMessage(msg);
        return this;
    }
    public MessageWrapper<T> wrapMessage(String[] message){
        setCode(message[0]);
        setMessage(message[1]);
        return this;
    }
}
