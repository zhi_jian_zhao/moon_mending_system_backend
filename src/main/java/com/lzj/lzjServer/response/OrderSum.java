package com.lzj.lzjServer.response;

/**
 * @author ZhiJian.Zhao
 * @since 2019-10-12
 * 订单统计
 */
public class OrderSum {
    private  String day;

    private  String orderCount;

    private String principal;

    private String commission;

    public String getTitle() {
        return day;
    }

    public String getTime() {
        return orderCount;
    }

    public String getContent() {
        return orderCount;
    }

    public String getImg() {
        return commission;
    }
}
