package com.lzj.lzjServer;

import com.lzj.lzjServer.request.OrderByUsername;
import com.lzj.lzjServer.request.OrderParams;
import com.lzj.lzjServer.request.SettlementParams;
import com.lzj.lzjServer.response.MessageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ZhiJian.Zhao
 * @since 2019-02-17
 * 订单结算
 */
@RestController
@RequestMapping("/api/v1")
public class BillSettlementController {
    @Autowired
    private BillSettlementRepository billSettlementRepository;

    /**
     * 添加用户账单
     * @param settlementParams
     * @return
     */
    @PostMapping(value = "/settlement")
    public BillSettlement settlementAdd(@RequestBody SettlementParams settlementParams) {
        BillSettlement settlement = new BillSettlement();
        settlement.setUsername(settlementParams.getUsername());
        settlement.setSettlementDay(settlementParams.getSettlementDay());
        return billSettlementRepository.save(settlement);
    }

    /**
     * 更新用户账单
     * @param settlementParams
     * @return
     */
    @PutMapping(value = "/settlement/{id}")
    public BillSettlement settlementUpdate(@PathVariable("id") Integer id,
                                           @RequestBody SettlementParams settlementParams) {
        BillSettlement settlement = new BillSettlement();
        settlement.setId(id);
        settlement.setUsername(settlementParams.getUsername());
        settlement.setSettlementDay(settlementParams.getSettlementDay());
        return billSettlementRepository.save(settlement);
    }

    /**
     * 删除一个账单
     * @param id
     */
    @DeleteMapping(value = "/settlement/{id}")
    public MessageWrapper orderDelete(@PathVariable("id") Integer id) {
        billSettlementRepository.deleteById(id);

        MessageWrapper messageWrapper = new MessageWrapper();
        messageWrapper.wrapMessage("200", "Success");
        return messageWrapper;
    }

    /**
     * 通过用户名查找
     * @param username
     * @return
     */
    @GetMapping(value = "/settlement/{username}")
    public BillSettlement orderListByUsername(@PathVariable("username") String username) {
        return billSettlementRepository.findByUsername(username);
    }
}
