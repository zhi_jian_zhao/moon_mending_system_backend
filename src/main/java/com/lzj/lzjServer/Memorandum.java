package com.lzj.lzjServer;

/**
 * @author ZhiJian.Zhao
 * @since 2019-03-06
 * 备忘录实体
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
@Entity
public class Memorandum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 要求数据库选择自增方式
    private  Integer id;

    private  String title;

    private  String time;

    private String content;

    private String img;

    public Memorandum() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
